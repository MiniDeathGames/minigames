﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace miniDeathGames
{
    public class Smites
    {
        Dictionary<string, string> smitesDictionnary = new Dictionary<string, string>();
        string line;
        char collumnSeparator = ';';

        public Dictionary<string, string> getSmites()
        {
            var assembly = Assembly.GetExecutingAssembly();

            string resourceName = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith("SmiteList.txt"));

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))

            using (StreamReader reader = new StreamReader(stream))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    int keyLenght = line.IndexOf(collumnSeparator);
                    smitesDictionnary.Add(line.Substring(0, keyLenght), line.Substring(keyLenght + 1, line.Length - keyLenght - 1));
                }
            }

            return smitesDictionnary;
        }

        public List<string> getSmitesFilesPaths()

        {
            if (smitesDictionnary.Count == 0) getSmites();

            List<string> listKeys = new List<string>(); 

            foreach (KeyValuePair<string, string> item in smitesDictionnary)
            {
                listKeys.Add(item.Value);
            }

            return listKeys;
        }

        public List<string> getSmitesNames()
        {
            if (smitesDictionnary.Count == 0) getSmites();

            List<string> listNames = new List<string>();

            foreach (KeyValuePair<string, string> item in smitesDictionnary)
            {
                listNames.Add(item.Key);
            }

            return listNames;
        }
    }
}
