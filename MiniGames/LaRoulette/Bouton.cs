﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miniDeathGames
{
    class Buton
    {
        private byte btnWidth;
        private byte btnHeight = 3;

        private string[] btn = new string[3]       {"────────────────",
                                                    "|              |",
                                                    "────────────────" };

        private byte cooX;
        private byte cooY;

        private string text;

        public byte BtnWidth {get => btnWidth;}
        public byte BtnHeight { get => btnHeight; }

        public string Text { get=>text; }

        public Buton()
        {
            btnWidth = Convert.ToByte(btn[0].Length);
        }

        public void setCoords(byte cooX, byte cooY)
        {
            this.cooX = cooX;
            this.cooY = cooY;
        }

        public void WriteButton(string text, byte textLine)
        {
            this.text = text;
            for(byte i = 0; i < btn.Count(); i++)
            {
                Console.SetCursorPosition(cooX, cooY + i);
                Console.Write(btn[i]);
            }

            Console.SetCursorPosition(cooX + btnWidth/2 - text.Length/2, cooY + textLine);
            Console.Write(text);
        }
    }
}
