﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace miniDeathGames
{
    class Screen
    {
        public static void setupScreen()
        {

        }

        public static void screenTransition()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;

            for (byte x = 0; x < Console.WindowWidth + Console.WindowWidth / 3; x++)
            {
                for (byte y = 0; y < Console.WindowHeight; y++)
                {
                    if (x - (y / 3) >= 0 && (x - (y / 3) < Console.WindowWidth))
                    {
                        Console.SetCursorPosition(x - (y / 3), y);
                        Console.Write("█");
                    }

                    if (y % 3 == 0)
                        Thread.Sleep(1);
                }
            }
        }
    }
}
