﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace miniDeathGames
{
    class Roulette
    {
        private const byte BUTTONNUMBER = 6;
        private const byte MARGINBUTTONY = 5;
        private const double MARGINLEFTMULTIPLICATOR = 1.1;
        private const byte SPACEBUTTON = 7;
        private const byte SPACEBUTTONX = 10;
        private const byte BUTTONARRAYHEIGHT = 2;
        private const byte BUTTONARRAYLENGTH = BUTTONNUMBER / BUTTONARRAYHEIGHT;
        private const byte MINROLLLENGHT = 19;
        private const byte MAXROLLLENGHT = 23;
        private const double FIRSTPARTPOW = 2;
        private const double SECONDPARTPOW = 2.2;
        private const byte SECONDPARTBEGINNING = 19;
        private const byte BASESLEEPTIME = 50;
        private const byte SMITESELECTEDSLEEP = 200;
        private const byte SMITESELECTEDCOUNT = 5;

        private Dictionary<string, string> smiteDictionnary = new Dictionary<string, string>();

        private List<string> smiteNames = new List<string>();

        private List<string> btnText = new List<string>();

        private Buton[] buttons = new Buton[BUTTONNUMBER];
        private List<byte> bytRdm = new List<byte>();

        private Smites smiteGetter = new Smites();

        private Random rnd = new Random();

        private string selectedSmitePath;

        public Roulette()
        {
            Console.Clear();
            GetSmites();
            AddRandomText();
            setByteRdm();
            setButtonCoord();
            WheelTurnRound();
            Screen.screenTransition();

            while (true)
                Console.ReadKey(true);
        }

        private void GetSmites()
        {
            smiteDictionnary = smiteGetter.getSmites();
            smiteNames = smiteGetter.getSmitesNames();
        }

        private void setButtonCoord()
        {
            byte counter = 0;

            for (byte y = 0; y < BUTTONARRAYHEIGHT; y++)
            {
                for (byte x = 0; x < BUTTONARRAYLENGTH; x++)
                {
                    buttons[counter] = new Buton();
                    buttons[counter].setCoords(Convert.ToByte(Console.WindowWidth / 2 - (buttons[counter].BtnWidth + SPACEBUTTONX) * BUTTONARRAYLENGTH + (SPACEBUTTONX + buttons[counter].BtnWidth) * (x + MARGINLEFTMULTIPLICATOR) + buttons[counter].BtnWidth),
                                                Convert.ToByte(MARGINBUTTONY + y * SPACEBUTTON));
                    buttons[counter].WriteButton(btnText[0], 1);
                    btnText.RemoveAt(0);
                    counter++;
                }
            }
        }

        private void setByteRdm()
        {
            for (byte y = 0; y < BUTTONNUMBER; y++)
            {
                bytRdm.Add(y);
            }
        }

        private void AddRandomText()
        {
            Dictionary<string, string> tempSmiteDictionnary = smiteDictionnary;

            for (byte i = 0; i < BUTTONNUMBER; i++)
            {
                int tempRdm = rnd.Next(0, tempSmiteDictionnary.Count);
                btnText.Add(smiteNames[tempRdm]);
                tempSmiteDictionnary.Remove(smiteNames[tempRdm]);
                smiteNames.RemoveAt(tempRdm);
            }
        }

        private void WheelTurnRandom()
        {
            int firstRdm = 0;
            int tempRdm = 1;
            int baseI = rnd.Next(1, BUTTONNUMBER);

            bytRdm.Remove(Convert.ToByte(firstRdm));

            for (int i = baseI; i < rnd.Next(MINROLLLENGHT, MAXROLLLENGHT) + baseI; i++)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                buttons[firstRdm].WriteButton(buttons[firstRdm].Text, 1);

                Console.ForegroundColor = ConsoleColor.White;
                buttons[tempRdm].WriteButton(buttons[tempRdm].Text, 1);

                tempRdm = firstRdm;

                firstRdm = bytRdm[rnd.Next(0, bytRdm.Count)];

                bytRdm.Add(Convert.ToByte(tempRdm));

                bytRdm.Remove(Convert.ToByte(firstRdm));

                Console.SetCursorPosition(0, 0);

                if (i < SECONDPARTBEGINNING)
                    Thread.Sleep(BASESLEEPTIME + Convert.ToInt32(Math.Pow(i, FIRSTPARTPOW)));
                else
                    Thread.Sleep(BASESLEEPTIME + Convert.ToInt32(Math.Pow(i, SECONDPARTPOW)));
            }
        }

        private void WheelTurnRound()
        {
            int baseI = rnd.Next(1, BUTTONNUMBER);
            int maxValue = rnd.Next(MINROLLLENGHT, MAXROLLLENGHT);

            for (int i = baseI; i < maxValue + baseI; i++)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                buttons[i % 6].WriteButton(buttons[i % BUTTONNUMBER].Text, 1);

                Console.ForegroundColor = ConsoleColor.White;
                buttons[(i - 1) % BUTTONNUMBER].WriteButton(buttons[(i - 1) % BUTTONNUMBER].Text, 1);

                selectedSmitePath = smiteDictionnary.FirstOrDefault(x => x.Value == buttons[(i - 1) % BUTTONNUMBER].Text).Key;

                Console.SetCursorPosition(0, 0);

                if (i < SECONDPARTBEGINNING)
                    Thread.Sleep(BASESLEEPTIME + Convert.ToInt32(Math.Pow(i, FIRSTPARTPOW)));
                else
                    Thread.Sleep(BASESLEEPTIME + Convert.ToInt32(Math.Pow(i, SECONDPARTPOW)));

                if (maxValue + baseI - 1 == i)
                {
                    smiteSelected(buttons[i % 6]);
                }
            }
        }

        private void smiteSelected(Buton button)
        {
            for (int i = 0; i < SMITESELECTEDCOUNT; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                button.WriteButton(button.Text, 1);
                Thread.Sleep(SMITESELECTEDSLEEP);
                Console.ForegroundColor = ConsoleColor.Cyan;
                button.WriteButton(button.Text, 1);
                Thread.Sleep(SMITESELECTEDSLEEP);
            }
        }
    }
}
