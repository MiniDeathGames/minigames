﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace miniDeathGames
{
    class Titlescreen
    {
        private bool isLabelShowed = false;
        private const byte labelMarginTop = 17;

        private const short timerInterval = 900;
        private const byte titleMarginTop = 5;

        private string[] title = new string[]     {"_______                                       _ ",
                                                    "| ___ (_)                                    | |",
                                                    "| |_/ /_  ___ _ ____   _____ _ __  _   _  ___| |",
                                                    "| ___ \\ |/ _ \\ '_ \\ \\ / / _ \\ '_ \\| | | |/ _ \\ |",
                                                    "| |_/ / |  __/ | | \\ V /  __/ | | | |_| |  __/_|",
                                                    "\\____/|_|\\___|_| |_|\\_/ \\___|_| |_|\\__,_|\\___(_)"};

        private string[] label = new string[] {"──────────────────────────",
                                               "| Appuyez sur une touche |",
                                               "|       pour entrer      |",
                                               "──────────────────────────"};

        private string subTitle = "A la roullette qui va faire des trucs à ton PC !";

        Timer timer = new Timer();

        public Titlescreen()
        {
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);

            WriteWindowsBorders();
            WriteTitlescreen();

            timer.Interval = timerInterval;
            timer.Elapsed += OnTimedEvent;
            timer.Start();

            Console.ReadKey(true);

            timer.Stop();
            Menu.ShowMenu();
        }

        private void WriteTitlescreen()
        {
            for(byte i = 0; i < title.Length; i++)
            {
                Console.SetCursorPosition(Console.BufferWidth/2 - (title[0].Length / 2), titleMarginTop+i);
                Console.Write(title[i]);
            }

            Console.SetCursorPosition(Console.BufferWidth / 2 - (title[0].Length / 2), titleMarginTop + title.Count() + 1);
            Console.Write(subTitle);
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            isLabelShowed = !isLabelShowed;

            for (byte i = 0; i < label.Length; i++)
            {
                Console.SetCursorPosition(Console.BufferWidth / 2 - (label[0].Length / 2), labelMarginTop + i);

                if (isLabelShowed)
                    Console.Write(label[i]);
                else
                    for (byte x = 0; x < label[0].Length; x++)
                        Console.Write(" ");
            }

            Console.SetCursorPosition(0, 0);
        }

        private void WriteWindowsBorders()
        {
            for (byte x = 0; x < 2; x++)
            {
                for (byte i = 0; i < Console.WindowWidth-1; i++)
                    Console.Write("█");

                Console.SetCursorPosition(0, Console.WindowHeight-1);
            }

            for (byte x = 0; x < 2; x++)
            {
                for (byte i = 0; i < Console.WindowHeight-1; i++)
                {
                    Console.SetCursorPosition(x * (Console.WindowWidth - 1), i);
                    Console.Write("█");
                }
            }
        }
    }
}
